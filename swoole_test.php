<?php
$iCount = 0;
$http = new swoole_http_server("127.0.0.1", 9091);
$http->set(array(
    'daemonize' => 1,
));
$http->on('request', function ($request, $response) {
    global $iCount;
    $iCount++;
    $response->end($iCount);
});
$http->start();